<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\NextcloudToDataverse\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
namespace OCA\DataverseBridge\AppInfo;

return [
    'routes' => [
	  /// ['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
     ///  ['name' => 'page#do_echo', 'url' => '/echo', 'verb' => 'POST'],
     /*  [
            'name' => 'Server#listServers',
            'url' => '/servers',
            'verb' => 'GET'
        ],*/
        [
            'name' => 'server#saveServers',
            'url' => '/servers',
            'verb' => 'POST'
        ],
        [
            'name' => 'server#uploadLimit',
            'url' => '/servers/uploadlimit',
            'verb' => 'POST'
        ],
        [
            'name' => 'Server#deleteServer',
            'url' => '/servers/{id}',
            'verb' => 'DELETE'
        ],
        [
            'name' => 'Server#getServerUrl',
            'url' => '/servers/getserverurl',
            'verb' => 'GET'
        ],
        [
            'name' => 'server#getServers',
            'url' => '/servers/getservers',
            'verb' => 'GET'
        ],
        
        [
            'name' => 'token#saveToken',
            'url' => '/tokens',
            'verb' => 'POST'
        ],
        [
            'name' => 'token#getToken',
            'url' => '/tokens/gettoken',
            'verb' => 'GET'
        ],
        [
            'name' => 'token#deleteToken',
            'url' => '/tokens/{id}',
            'verb' => 'DELETE'
        ],
        [
            'name' => 'dataverse#all',
            'url' => '/dataverse/all',
            'verb' => 'GET'
        ],
        [
            'name' => 'dataset#all',
            'url' => '/dataset/all',
            'verb' => 'GET'
        ],
        [
            'name' => 'dataset#writable',
            'url' => '/dataset/writable',
            'verb' => 'GET'
        ],
        [
            'name' => 'dataset#exist',
            'url' => '/dataset/exist',
            'verb' => 'POST'
        ],
        [
            'name' => 'dataset#sentFilenames',
            'url' => '/dataset/sent',
            'verb' => 'POST'
        ],
        
        [
            'name' => 'publish#publishFiles',
            'url' => '/publish/publishfiles',
            'verb' => 'POST'
        ],
    ]
];
