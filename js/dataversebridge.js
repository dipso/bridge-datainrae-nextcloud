
(function () {
    
    OCA.DataverseBridge = OCA.DataverseBridge || {};

    /**
     * @namespace
     */
    OCA.DataverseBridge.Util = {
        /**
         * Initialize the dataversebridge plugin.
         *
         * @param {OCA.Files.FileList} fileList file list to be extended
         */
        attach: function (fileList) {
            if (fileList.id === 'trashbin' || fileList.id === 'files.public') {
                return;
            }
            var fileActions = fileList.fileActions;

            fileActions.registerAction(
                {
                    name: 'Dataverse',
                    displayName: 'Dataverse',
                    mime: 'file',
                    permissions: OC.PERMISSION_READ,
                    icon: OC.imagePath('dataversebridge', 'dataverseicon'),
                    actionHandler: function (fileName) {
                        fileList.showDetailsView(fileName, 'dataverseBridgeTabView');
                    },
                }
            );
            fileList.registerTabView(new OCA.DataverseBridge.DataverseBridgeTabView('DataverseBridgeTabView',{order: -30}));
        }
    };

})();

OC.Plugins.register('OCA.Files.FileList', OCA.DataverseBridge.Util);