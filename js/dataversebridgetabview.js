(function() {
    
    var TEMPLATE = '<div id="dataverseBridgeTabView" class="dataversebridgeform">' +
		'<p><b>Serveur Data INRAE:</b></p><div id="serverSelector"><select name="servers" id="servers-select" style="width: 100%;"><option>--choissisez votre serveur--</option></select></div>' +
        '<div id="datasets" style="display: none;"><p id="datasetlabel"><b>Dataset:</b></p><input id="datasetinput" type="text" name="dataset" autocomplete="off" list="dataset_list" style="width: 100%;"><datalist id="dataset_list" autocomplete="off"></datalist></div>' +
        '<input type="button" value="Envoyer" id="publish_button" style="horizontal-align: middle; margin:auto;  width: 50%; text-align: center; margin-top: 15px; display: none;">' +
		'<div class="errormsg" id="dataversebridge_errormsg">BeforeInit</div> </div>';
    
    var dataverses = [];
    var datasets = [];

    var serverName;  var serverId="";
    var servers = [];

    var util = new OCA.DataverseBridge.Util();
    var request = new OCA.DataverseBridge.Request();

     
    var DataverseBridgeTabView = OCA.Files.DetailTabView.extend(
    {
        id: 'dataverseBridgeTabView',
        className: 'dataverseBridgeTabView tab',

        _label: 'dataverse',

        _loading: false,

		//_publish_buton_disabled: false,


        initialize: function() {
            OCA.Files.DetailTabView.prototype.initialize.apply(this, arguments);
            this.collection = new OCA.DataverseBridge.DataverseBridgeCollection();
            this.collection.setObjectType('files');
            this.collection.on('request', this._onRequest, this);
            this.collection.on('sync', this._onEndRequest, this);
            this.collection.on('update', this._onChange, this);
            this.collection.on('error', this._onError, this);
			this._error_msg = "";
            request.getServers(servers);
            

        },

        events: {
        },


        getLabel: function() {
            return t('dataversebridge', 'Dataverse');
        },

        getIcon: function() {
           return 'icon-dataversebridge';
        },

        nextPage: function() {
        },

        _onClickShowMoreVersions: function(ev) {
        },

        _onClickRevertVersion: function(ev) {
        },

        _toggleLoading: function(state) {
        },

        _onRequest: function() {
        },

        _onEndRequest: function() {

        },

        _onAddModel: function(model) {
        },

        template: function(data) {
            return TEMPLATE;
        },

        itemTemplate: function(data) {
        },

        setFileInfo: function(fileInfo) {
            if (fileInfo) {
                this.fileInfo = fileInfo;
                this.render(fileInfo);
            }
        },

        _formatItem: function(version) {
        },

        checkToken: function() {
            if (!this.tokens[$('#ddServerSelector').val()]) {
                $(dataversebridge_errormsg).html('Please set Dataverse API token in Dataverse settings');
                $(dataversebridge_errormsg).show();
            } else {
                $(dataversebridge_errormsg).hide();
            }
        },

        /**
         * Renders this details view
         */
        render: function(fileInfo) {
            this.$el.html(this.template()).ready(function(){
            //document.getElementById('publish_button').disabled = true;
            console.log(document.getElementById('publish_button'));
            console.log("servers from reder ");
            console.log(servers);
            request.sentFilenames(fileInfo, servers);
            util.setOptionsFromArray(servers, 'servers-select');
            $('#servers-select').change(function(){
                serverName = $('#servers-select').find(":selected").val();
                //if(serverName == '--Unspecified--'){
                    console.log("changed");
                    var tokMsg = document.getElementById('token_msg');
                    var duplicatSent = document.getElementById('duplicat_sent');
                    var uploadSize  = document.getElementById('upload_size');
                    var datasetError  = document.getElementById('dataset_error');
                    var datasetsDisplayed = (document.getElementById('datasets').style.display == 'block') ? true : false;
                    if(tokMsg != undefined){
                        tokMsg.style.display = 'none';
                    }
                    if(duplicatSent != undefined){
                        duplicatSent.style.display = 'none';
                    }
                    if(uploadSize != undefined){
                        uploadSize.style.display = 'none';
                    }
                    
                    if(datasetsDisplayed){
                        document.getElementById('datasetinput').value = '';
                        document.getElementById('datasets').style.display = 'none';
                    }
                    if(datasetError != undefined){
                        datasetError.style.display = 'none';
                    }
                    console.log("check disabling");
                    if(!document.getElementById('publish_button').style.display != 'none'){
                        console.log("disabling");
                        document.getElementById('publish_button').style.display = 'none';
                    }
                    console.log("unspecified");
                //}
                //else
                if(serverName != '--choissisez votre serveur--')
                {
                    serverId = servers[serverName]['serverId'];
                    request.getUserToken(serverName, serverId);
                    if(localStorage.getItem('userToken') != 'null'){
                        var sizeOk = request.checkSizeLimit(servers[serverName]['serverId'], fileInfo, serverName);
                        if(sizeOk){
                            var datasets_name_doi = request.getWritableDatasets(localStorage.getItem('userToken'), datasets,  servers[serverName]['serverId'], servers[serverName]['serverUrl'], serverName);
                            console.log(datasets_name_doi);
                            util.showOptions(datasets_name_doi.sort());
                            $('#datasetinput').bind('click keyup', function(event){

                                if(datasets_name_doi.includes(this.value) && (event.keyCode === undefined || event.keyCode === 13 || event.keyCode === 1)){
                                    console.log("value is " + this.value);
                                    var [datasetName, datasetId, state] = this.value.split('   -   ');
                                    console.log("state is " + state);
                                    request.wasFilesSent(serverId, serverName, datasetName, fileInfo, datasetId);
                                    $(publish_button).unbind('click').bind('click',{'param': fileInfo, 'serverUrl':  servers[serverName]['serverUrl'], 'serverId':  servers[serverName]['serverId'], 'datasetId': datasetId, 'state': state, 'servers': servers}, request.uploadAction);
                                }
                                else
                                {
                                    var uplBtn = document.getElementById('publish_button');
                                    if(uplBtn != undefined){
                                        uplBtn.style.display = 'none';

                                    }
                                    var dupMsg = document.getElementById('duplicat_sent');
                                    if(dupMsg != undefined){
                                        dupMsg.style.display = 'none';

                                    }
                                    
                                }
                            });
                        }
                    }
                }  
            });
        });
        },

        /**
         * Returns true for files, false for folders.
         *
         * @return {bool} true for files, false for folders
         */
        canDisplay: function(fileInfo) {
            if (!fileInfo) {
                return false;
            }
            return !fileInfo.isDirectory();
        },

        processData: function(data) {
			this._publish_button_disabled = data['error'];
			this._error_msg = data['error_msg'];
			this._dv_title = data['title'];
		}
    });

    OCA.DataverseBridge = OCA.DataverseBridge || {};

    OCA.DataverseBridge.DataverseBridgeTabView = DataverseBridgeTabView;
    
})();

