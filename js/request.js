(function() {
    var token_msg = '<div id="token_msg" style="font-size: 12px;">Le jeton API du serveur SERVERNAME n\'est pas encore configuré. \
                Pour le configurer:\
                <p>1. Générer le jeton dans Dataverse en suivant cette <a title="doc_dv_tok_generate" href="https://ist.blogs.inrae.fr/datainrae-guide/sauthentifier-et-gerer-son-compte/#Espace_Personnel" target="_blank" rel="noopener noreferrer">documentation</a></p>\
                <p>2. Copier le jeton généré et le renseigner <a href="/settings/user/dataversebridge">ici</a></p></div>';
    
    var duplicate_sent_msg = '<div id="duplicat_sent">Le fichier "FILE" de même contenu existe déjà dans le dataset <a href="LINK">DATASET</a></div>';
    var uploadSizeMsg = '<div id="upload_size">Le fichier "FILE" ne peut pas être envoyé à DATAINRAE SERVER car sa taille est supérieure à la limite autotisée(LIMIT)</div>';
    var filenamePresent = '<div id="filename_present"></div>';
    var datasetError = '<div id="dataset_error"></div>';

    var Request = function(){
        this.getServers = function(servers) {
            var serversPath = OC.generateUrl('/apps/dataversebridge/servers/getservers');
            $.ajax({
                type: 'GET',
                url: serversPath,
                async: false
            }).done(function(response){
                for(var serverName  in response){
                    servers[serverName] = response[serverName];
                }
                console.log("serversssss from ajax ");
                console.log(servers);
            }).fail(function(data){
                console.log("Failure can not fetch servers info " + data.toString());
            });
        };

        this.getUserToken = function(serverName, serverId){
            var baseUrl = OC.generateUrl('/apps/dataversebridge/');
            var tokenUrl = baseUrl+'tokens/gettoken';
            localStorage.setItem('userToken', null );
            $.ajax({
                data: {'serverId': serverId},
                type: 'GET',
                url: tokenUrl,
                async: false
            }).done(function(data) {
                localStorage.setItem('userToken', data.token );
                var tok_msg = document.getElementById('token_msg')
                if(tok_msg != undefined){
                    tok_msg.style.display = 'none';
                }
            }).fail(function(data){
                 console.log("***failure to get user token" + data.toString());
                 $('#serverSelector').after(token_msg.replace("SERVERNAME", serverName));
                 $('#token_msg').css('color', 'red');
                 document.getElementById('token_msg').style.display = 'block';
            });
        };

        this.getDataverses = function(serverUrl, dataverses) {
            var baseUrl = OC.generateUrl('/apps/dataversebridge/dataverse');
            var url_path= baseUrl+'/all';
            result = "";
            $.ajax({
                data: {'serverUrl': serverUrl},
                type: 'GET',
                url: url_path,
                async: true
            }).done(function(data){
                $.each(data.dataverses, function(index, dataverse){
                    dataverses.push(dataverse);
                });
    
            }).fail(function(data){
                console.log("failure " + data.toString());
                
            });
        };


        
     this.getDatasets = function(serverUrl, datasets) {
        var baseUrl = OC.generateUrl('/apps/dataversebridge/dataset');
        var url_path= baseUrl+'/all';
        $.ajax({
            data: {'serverUrl': serverUrl},
            type: 'GET',
            url: url_path,
            async: true
        }).done(function(data){
            $.each(data.datasets, function(index, dataset){
                datasets.push(dataset);
            });            

        }).fail(function(data){
            console.log("failure " + data.toString());
            
        });
     };

     this.getWritableDatasets = function(userToken, datasets, serverId, serverUrl, serverName) {
        var baseUrl = OC.generateUrl('/apps/dataversebridge/dataset');
        var url_path= baseUrl+'/writable';
        datasets.length = 0; datasets_name_doi = []; uniqueDatasets = [];
        $.ajax({
            data: {'serverUrl': serverUrl, 'serverId': serverId, 'userToken': userToken},
            type: 'GET',
            url: url_path,
            async: false
        }).done(function(data){
            if(data.success){
            $.each(data.datasets, function(index, dataset){
                datasets[index] = { name: dataset[0], doi: dataset[1], isPublished: dataset[2]};
                uniqueDatasets = getLatestDatasets(uniqueDatasets, datasets[index]); 
            });
            document.getElementById("datasets").style.display = "block";
            //document.getElementById("datasetinput").style.display = "block";
        } else 
        {
            if($('#dataset_error').length === 0)
                $('#serverSelector').after(datasetError);
            var datasetErrElmt = document.getElementById('dataset_error');
            if(data.message.includes("Please login.")){
                datasetErrElmt.innerHTML = '<p>Le jeton API du serveur SERVER est invalide.</p>'.replace("SERVER", serverName) +
                '<p>Pour le regénérer dans Dataverse, suivre cette <a href="https://ist.blogs.inrae.fr/datainrae-guide/sauthentifier-et-gerer-son-compte/#Espace_Personnel">documentation</a></p>' +
                '<p>Pour le corriger ou sauvegarder le nouveau jeton, le renseigner <a href="/settings/user/dataversebridge">ici</a></p>'
            }
            //datasetErrElmt.innerHTML = data.message;
            $('#dataset_error').css('color', 'red');
            datasetErrElmt.style.display = 'block';

        }
        }).fail(function(data){
            console.log("failed to get writable datasets " + data.toString());
            
        });
        console.log("Unique is ");
        console.log(uniqueDatasets);
        for (var key in uniqueDatasets) {
            state = uniqueDatasets[key] ? 'published' : 'draft' ;
            datasets_name_doi.push(key + '   -   ' + state);
        }
        
        return datasets_name_doi;
     };

    function getLatestDatasets(uniqueDatasets, dataset)
    {
            if(!( dataset.name + '   -   ' + dataset.doi in uniqueDatasets))
            { 
                uniqueDatasets[dataset.name + '   -   ' + dataset.doi] = dataset.isPublished;

            }
            else{
                uniqueDatasets[dataset.name + '   -   ' + dataset.doi] = false;
            }
        return uniqueDatasets;
    };
    this.checkSizeLimit = function(serverId, fileInfo, serverName){
        var filesId = getFilesId(fileInfo);
        var baseUrl = OC.generateUrl('/apps/dataversebridge');
        var uploadLimUrl = baseUrl + '/servers/uploadlimit';
        var sizeOk = true;
        
        $.ajax({
            data: {'serverId': serverId, 'filesId': filesId},
            type: 'POST',
            url: uploadLimUrl,
            async: false
        }).done(function(data){
            if(data.limitExeeded.length === 0){
                var uploadSizeElmt = document.getElementById('upload_size');
                if(uploadSizeElmt != undefined){
                    uploadSizeElmt.style.display = 'none';
                }
                sizeOk = true;
            }else{
                sizeOk = false;
                var ExceededFile = "";
                for (var fileid in data.limitExeeded) {
                    if (data.limitExeeded.hasOwnProperty(fileid)){
                        console.log(data[fileid]);
                        ExceededFile = ExceededFile + data.limitExeeded[fileid] + ", ";
                    } 
                }
                $('#serverSelector').after(uploadSizeMsg.replace("FILE", ExceededFile.substring(0, ExceededFile.length-2)).replace("SERVER", serverName).replace("LIMIT", formatBytes(data.serverLimit)));
                $('#upload_size').css('color', 'red');
                document.getElementById('upload_size').style.display = 'block';
            }

        }).fail(function(data){
            console.log("failed to check if file size is accepted by the server");
            console.log(data);
        });
        return sizeOk;
    };

     this.wasFilesSent = function (serverId, serverName, datasetName, fileInfo, datasetId){
        var filesId = getFilesId(fileInfo);
        console.log(filesId);
        var baseUrl = OC.generateUrl('/apps/dataversebridge/dataset');
        var url_path= baseUrl+'/exist';
        datasets.length = 0;
        $.ajax({
            data: {'serverId': serverId, 'datasetName': datasetName, 'filesId': filesId, 'datasetId': datasetId},
            type: 'POST',
            url: url_path,
            async: true
        }).done(function(data){
            if(data.length === 0){
                var duplicate_msg = document.getElementById('duplicat_sent')
                if(duplicate_msg != undefined){
                    duplicate_msg.style.display = 'none';
                }
                document.getElementById('publish_button').style.display = 'block';

            }else {
                var sentFile = "";
                for (var fileid in data) {
                    if (data.hasOwnProperty(fileid)){
                        console.log(data[fileid]);
                        sentFile = sentFile + data[fileid] + ", ";
                    }     
                }
                if(duplicate_msg == undefined)
                    $('#datasets').after(duplicate_sent_msg.replace("FILE", sentFile.substring(0, sentFile.length-2)).replace("DATASET", datasetName).replace("LINK", getLinkTodatasest(serverName, datasetId)));
                $('#duplicat_sent').css('color', 'red');
                document.getElementById('duplicat_sent').style.display = 'block';
                var upl_btn = document.getElementById('publish_button');
                if(upl_btn != undefined){
                    upl_btn.style.display = 'none';
                }
            }

        }).fail(function(data){
            console.log("failed to check if file(s) are already sent");
            console.log(data); 
        });
        
     };


     this.sentFilenames = function (fileInfo, servers){
        var filesId = getFilesId(fileInfo);
        var msgs = '';
        console.log(filesId);
        var baseUrl = OC.generateUrl('/apps/dataversebridge/dataset');
        var url_path= baseUrl+'/sent';
        $.ajax({
            data: {'filesId': filesId},
            type: 'POST',
            url: url_path,
            async: false
        }).done(function(data){
            if(data.length === 0){
                var duplicate_msg = document.getElementById('duplicat_sent');
                if(duplicate_msg != undefined){
                    duplicate_msg.style.display = 'none';
                }
            }else {
                var msg = '<p>Le fichier "FILE" est present dans ce <a style="color: blue; text-decoration: underline; text-decoration-color: blue;" href="LINK" target="_blank" rel="noopener noreferrer">dataset</a></p>'
                console.log("file present");
                console.log(data);
                for (var fileid in data) {
                    if (data.hasOwnProperty(fileid)){
                        var serverName = Object.keys(servers).find(key => servers[key]['serverId'] == data[fileid]['serverId']);
                        var datasetLink = getLinkTodatasest(serverName, data[fileid]['datasetId']);
                        msgs = msgs + msg.replace("FILE", data[fileid]['filename']).replace("LINK", datasetLink);
                    }     
                }
                $('#serverSelector').before(filenamePresent);
                document.getElementById('filename_present').innerHTML = msgs;
                document.getElementById('filename_present').style.display = 'block';
            }

        }).fail(function(data){
            console.log("failed to check if file(s) are present in a dataset");
            console.log(data);
        });
        
     };

     this.uploadAction = function (event){
        initData = initializeUpload(event);
        var baseUrl = OC.generateUrl('/apps/dataversebridge/publish');
        var url_path= baseUrl+'/publishfiles';
        console.log("post data are ");
        console.log(initData.postData);

        $.ajax({
            data: initData.postData,
            type: 'POST',
            url: url_path,
            async: true
        }).done(function(data)
        {
            console.log("Uploaded response is ");
            console.log(data);
            var x = document.getElementsByClassName('dataversebridgeform')[0];
            x.style.display = 'none';
            data.forEach((response, index) => {
                showUploadResponse(response, index, initData.postData.datasetName, initData.postData.persistentId, initData.postData.serverId, event.data.servers);
                console.log("Uploaded response is " + response);
                console.log(response);
            });

        }).fail(function(data){
            console.log("failed to upload the file(s) " + JSON.stringify(data, null, 2));
        });
        
    };

    function initializeUpload(event){
        document.getElementById('publish_button').style.display = 'none';
        selectedFiles = FileList.getSelectedFiles();
        
        selected_dataset_name = document.getElementById("datasetinput").value.split('   -   ')[0];
        //persistentId = event.data.datasets.filter(dataset => selected_dataset_name.includes(dataset.name)).map(dataset => dataset.doi);
        userToken = localStorage.getItem('userToken');
        if (selectedFiles.length>0){
            fileIds = [];
            for (index in selectedFiles){
                fileIds.push(selectedFiles[index].id);
            }
        } else {
            fileInfo = event.data.param;
            fileIds = [fileInfo.id];
            
        }
        console.log("serveriddd " + event.data.serverId);
        return {'postData': {'serverUrl': event.data.serverUrl, 'serverId': event.data.serverId, 'userToken': userToken, 'fileIds': fileIds, 'persistentId':  event.data.datasetId, 'datasetName': selected_dataset_name, 'state': event.data.state}};
     };

     function getFilesId(fileInfo){
        selectedFiles = FileList.getSelectedFiles();
        if (selectedFiles.length>0){
            fileIds = [];
            for (index in selectedFiles){
                fileIds.push(selectedFiles[index].id);
            }
        } else {
            fileIds = [fileInfo.id];
            
        }
        console.log("ids are ");
        console.log(fileIds);
        return fileIds;
     };
     function formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';
    
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    
        const i = Math.floor(Math.log(bytes) / Math.log(k));
    
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
    function getLinkTodatasest(serverName, datasetId){
        var dataset_link = "";
        switch(serverName){
            case "PRE-PROD":
                dataset_link = "https://data-preproduction.inrae.fr/dataset.xhtml?persistentId=" + datasetId; break;
            default: 
                dataset_link = "https://doi.org/" + datasetId; break;
        }
        return dataset_link;
    }

    function showUploadResponse(response, index, dataset_name, datasetId, serverId, servers) {
        console.log("before show response");
        console.log(response);
        console.log(index);
        console.log("after show response");
        $(".dataversebridgeform").after('<div id="result"></div>');
        $("#result").attr('id','result_' + index);
        if(response.status === "OK")
        {           
            msg = '<div id="result_"'  + index + '>"' + response.data.files[0].dataFile.filename + '" a été déposé avec succès dans le  dataset <a href="LINK">' + dataset_name + '</a></div>';
            var serverName = Object.keys(servers).find(key => servers[key]['serverId'] == serverId);
            datasetUrl = getLinkTodatasest(serverName, datasetId);
            msg = msg.replace("LINK", datasetUrl);
            if(response.dup_file_msg)
                msg = msg.replace('</div>', '. Une version précédente de ce fichier existe déjà dans le même dataset</div>');
            color = 'green'
        } else
        {
            msg = '<div id="result_"'  + index + '>Error: '+ response.code + ' Message: ' +  'une erreur est apparue en envoyant le ficher "' + response.data.files[0].dataFile.filename +  '" vers le dataset <a href="'+ datasetUrl + '">' + dataset_name + '</a></div>'
            color = 'red'        
        }
        $('#result_' + index).html(msg);
        $('#result_' + index).css('color', color);
        document.getElementById('result_' + index).style.display = 'block';  
    };

};
    OCA.DataverseBridge = OCA.DataverseBridge || {};
    OCA.DataverseBridge.Request = Request;

})();