$(document).ready(
    function () {
        var baseUrl = OC.generateUrl('/apps/dataversebridge/servers');
        var servers_nbr = '<?php echo(count($servers)); ?>';
        console.log("nombre of servers is " + servers_nbr);
        document.getElementById("delete").style.display = servers_nbr > 0 ? "block" : "none";
        document.getElementById("send").style.display = servers_nbr > 0 ? "block" : "none";

        function saveChanges() {
            console.log("saving changes...");
            var names = $('[id^="name"]');
            var publishUrls = $('[id^="url"]');
            var uploadLimits = $('[id^="limitSize"]');
            var regex = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i;
            var data = [];
            for (i=0; i<names.length; i++) {
                var publishUrl = publishUrls[i].value;
                var uploadLimit = uploadLimits[i].value;
                data.push({name: names[i].value, publishUrl, uploadLimit});
                id = names[i].id.split("_")[1];
                if (id && id.length) {
                    data[i].id = id;
                }
                if (!regex.exec(publishUrl)) {
                    OC.msg.finishedSaving('#saving .msg', {
                        'status': 'failure',
                        'data': {
                            'message': `Invalid URL: ${publishUrl}`
                        }
                    })
                    return;
                }
            }
            OC.msg.startSaving('#saving .msg','Saving servers...');

            $.ajax({
                url: baseUrl,
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({servers: data})
            }).done(function(response) {
                if (response.length) {
                    const id = response[response.length-1].id;
                    console.log("done!!! " + id);
                    console.log("length " + response.length);
                    console.log("response " + response);
                    document.location.reload();
                    $('#add-server').show();
                    $('#delete').hide();
                    $('#send').hide();
                    $( "#dataverse_server" ).remove();
                }
                OC.msg.finishedSaving('#saving .msg', {
                    'status': 'success',
                    'data': {
                        'message': 'Saved'
                }});
            }).fail(function(response) {
                OC.msg.finishedSaving('#saving .msg', {
                    'status': 'failure',
                    'data': {
                        'message': 'Saving failed!'
                }});
                console.log("failing...");
            })
        }

        function deleteServer(event) {
            const id = event.target.id.split('_')[1];
            console.log("the id is " + id);
            if (id) {
                if (!window.confirm("Are you sure you want to delete the server? This action is irreversible." + id)) {
                    return;
                }
                OC.msg.startSaving('#saving .msg','Deleting...');
                $.ajax({
                    url: baseUrl+'/'+id,
                    type: 'DELETE',
                }).done(function(response) {
                    $('#dataverse_server_'+id).remove();
                    $('#delete').hide();
                    $('#send').hide();
                    OC.msg.finishedSaving('#saving .msg', {
                        'status': 'success',
                        'data': {
                            'message': 'Server deleted'
                    }});
                }).fail(function(response) {
                    OC.msg.finishedSaving('#saving .msg', {
                        'status': 'failure',
                        'data': {
                            'message': 'Deleting failed!'
                    }});
                })
            } else {
                $('#dataverse_server').remove();
                console.log("***removed");
                $('#add-server').show();
                $('#delete').hide();
                $('#send').hide();
            }
        }

        $("#send").on('click', function() {saveChanges();});
        $("[id^='#update_']").on('click', function() {saveChanges()});
        $("[id^='#delete_']").on('click', deleteServer);
        $('#delete').on('click', deleteServer);
       
        $('#add-server').on('click', function() {
            $('#add-server').before('<div id="dataverse_server"><p id="dataverseUrlField">\
                <input title="publish_baseurl" type="text" name="publish_baseurl"\
                id="url" placeholder="https://data.inrae.fr" style="width: 400px"/>\
                   <em>Publish URL</em></p>\
                <p id="dataverseNameField">\
                <input title="name" type="text" name="name"\
                id="name"\
                style="width: 400px"/>\
                <em>Server name</em></p>\
                <p id="dataverseLimitSize">\
                <input title="limitSize" type="text" name="limitSize"\
                   id="limitSize"\
                   style="width: 400px"\/>\
                   <em>server upload limit size</em></p></div>'
            );
            $('#add-server').hide();
            $('#delete').show();
            $('#send').show();
        
        })
    }
);