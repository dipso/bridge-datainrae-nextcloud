function saveUserToken(event)
{
    const ids = event.target.id.split('_'); //
    const server_id = ids[3];
    const token_id = ids[4];

    var token_value = document.getElementById("dataverse_apitoken_" + server_id).value;
    var data = {};
    if (token_id && token_id.length) {
        data.token_id = token_id;
    }
    data.server_id = server_id;
    data.token_value = token_value;
    
    OC.msg.startSaving('#lostpassword_'+server_id + '_' + token_id + ' .msg','Saving user token');
    result = {};
    $.ajax({
        url: baseUrl,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({token: data})
    }).done(function(response) {
        if( $("#dataverse_delete_apitoken_" + server_id + "_" + response.id).length == 0)
        {
            $('#dataverse_delete_apitoken_' + server_id + '_').attr("id","dataverse_delete_apitoken_" + server_id + "_" + response.id);
            $('#dataverse_save_apitoken_' + server_id + '_').attr("id","dataverse_save_apitoken_" + server_id + "_" + response.id);
        }

        OC.msg.finishedSaving('#saving .msg', {
            'status': 'success',
            'data': {
            'message': 'Saved'
        }});
    }).fail(function(response) {
        OC.msg.finishedSaving('#saving .msg', {
            'status': 'failure',
            'data': {
                'message': 'Saving failed!'
        }});
        console.log("failing...");
    });
}

function deleteAPIToken(event)
{
    const ids = event.target.id.split('_');
    const server_id = ids[3];
    console.log("server id is " + server_id);
    const token_id = ids[4];
    $.ajax(
        {
            type: 'DELETE',
            url:  baseUrl+'/'+token_id
        }
        ).done(
            function (result) {
                $('#dataverse_delete_apitoken_' + server_id + '_' + token_id).attr("id","dataverse_delete_apitoken_" + server_id + "_");
                $('#dataverse_save_apitoken_' + server_id + '_' + token_id).attr("id","dataverse_save_apitoken_" + server_id + "_");
                $('#dataverse_apitoken_' + server_id).val('');
                //$('#dataverse_apitoken_' + server_id).removeAttr('value');
            //console.log($('dataverse_apitoken_'));
                OC.msg.finishedSaving('#saving .msg', {
                    'status': 'success',
                    'data': {
                        'message': 'Token deleted'
                    }
                });
        }).fail(function(response) {
            OC.msg.finishedSaving('#saving .msg', {
                'status': 'failure',
                'data': {
                    'message': 'Delete token failed!'
            }});
        });
}
var baseUrl = OC.generateUrl('/apps/dataversebridge/tokens');
$(document).ready(
    function () {
        const saveButtons = $('[id^=dataverse_save_apitoken');
        const deleteButtons = $('[id^=dataverse_delete_apitoken');
        for (let i=0; i<saveButtons.length; i++) {
            $('#'+saveButtons[i].id).on('click', saveUserToken);
            $('#'+deleteButtons[i].id).on('click', deleteAPIToken);
        }
        //const saveButtons = $('[id^=dataverse_save_apitoken');
        //const deleteButtons = $('[id^=dataverse_delete_apitoken');
    }
);