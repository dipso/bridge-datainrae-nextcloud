
(function() {

    //var request = new OCA.DataverseBridge.Request();

    //test-2-test


    var Util = function()
    {
        this.setOptionsFromArray = function(options, selectId){
            console.log("*** Im invoked**");
            for(var ServerName in options){
                $('#' + selectId)
                    .append($("<option></option>")
                        .attr("value", ServerName)
                        .text(ServerName + '  --  ' + options[ServerName]['serverUrl']));
            }
        };

      this.showOptions = function(optionList){
         var options ='';
          for (var i = 0; i < optionList.length; i++) {
            options += '<option class="datasetOptions" value="' + optionList[i] + '" />';
          }
          var elmnt = document.getElementById('dataset_list');
          elmnt.innerHTML = options;
      };
    };

    OCA.DataverseBridge = OCA.DataverseBridge || {};
    OCA.DataverseBridge.Util = Util;

})();