<?php


namespace OCA\DataverseBridge\AppInfo;



use Closure;
use OCA\DataverseBridge\Model\ServerMapper;
use OCA\DataverseBridge\Model\TokenMapper;

use OCA\DataverseBridge\Notification\Notifier;

use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\IContainer;
use OCP\Util;

use OCP\Notification\IManager;
use OCP\User\Events;





class Application  extends App implements IBootstrap
{
    public const APP_ID = 'dataversebridge';
    /**
     * @param array(string) $urlParams a list of url parameters
     */
    public function __construct(array $urlParams = []) {
        parent::__construct(self::APP_ID, $urlParams);
    }

    public function register(IRegistrationContext $context): void {

        $container = $this->getContainer();
        $server = $container->getServer();
        
        $context->registerService(
            'ServerMapper',
            function () use ($server) {
                return new ServerMapper(
                    $server->getDatabaseConnection()
                );
            }

        );
        $context->registerService(
            'TokenMapper',
            function () use ($server) {
                return new TokenMapper(
                    $server->getDatabaseConnection()
                );
            }

        );

        $context->registerEventListener(
            BeforeUserDeletedEvent::class,
            UserDeletedListener::class
        );
    }

    public function boot(IBootContext $context): void {
        //$this->registerNavigationEntry();
        $this->loadScripts();
        $this->registerSettings();
        $context->injectFn(Closure::fromCallable([$this, 'registerNotification']));
    }

    private function registerNotification(IManager $notifications): void {
		$notifications->registerNotifierService(Notifier::class);
	}


    /**
     * Register Navigation Entry
     *
     * @return null
     */
    public function registerNavigationEntry()
    {
        $c = $this->getContainer();
        $server = $c->getServer();

        $navigationEntry = function () use ($c, $server) {
            return [
                'id' => $c->getAppName(),
                'order' => 101,
                'name' => 'Dataverse',
                'icon' => $server->getURLGenerator()
                    ->imagePath('dataversebridge', 'dataverseicon.svg'),
            ];
        };
        $server->getNavigationManager()->add($navigationEntry);
        return;
    }

    /**
     * Register Settings pages
     *
     * @return null
     */
    public function registerSettings()
    {
        return;
    }


    /**
     * Load additional javascript files
     *
     * @return null
     */
    public function loadScripts()
    {
        
        Util::addScript('files', 'detailtabview');
        Util::addScript('dataversebridge', 'dataversebridgecollection');
        Util::addScript('dataversebridge', 'util');
        Util::addScript('dataversebridge', 'request');
        Util::addScript('dataversebridge', 'dataversebridgetabview');
        Util::addScript('dataversebridge', 'dataversebridge');
        Util::addStyle('dataversebridge', 'dataversebridgetabview');
        
        return;
    }
}