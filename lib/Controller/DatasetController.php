<?php
namespace OCA\DataverseBridge\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use Psr\Log\LoggerInterface;
use OCA\DataverseBridge\Model\UploadedFileMapper;
use OC\Files\Filesystem;
use OCP\Files\IRootFolder;
use \OCP\Files\Node;
use \OCP\Files\Mount\IMountPoint;

class DatasetController extends Controller {

    private $userId;
    private $logger;
    private $mapper;

	public function __construct($AppName, IRequest $request, $UserId,  UploadedFileMapper $mapper, LoggerInterface $logger){
		parent::__construct($AppName, $request);
        $this->mapper = $mapper;
		$this->userId = $UserId;
        $this->logger = $logger;
	}

    public function log($message) {
        $this->logger->error($message, ['extra_context' => 'my extra context']);
    }

    public function getUrlContent($url, $userToken=null)
    {
        $ch = curl_init();
        $this->log("*** after init ***");
        if ($ch === false) {
            throw new Exception('failed to initialize');
        }
        
        $config = array(
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
        );
        
       
        curl_setopt_array($ch, $config);
        $this->log("*** after set option ***");

        $result = curl_exec($ch);
        $this->log("*** after exec ***");
        curl_close($ch);
        if (!$result) {
            $this->log("*** false ***");
            $this->log(curl_error($ch), curl_errno($ch));
            return false;
        } else {
            $this->log("*** result *** " . $result);
            return $result;
        }
    }
      /**
     * request endpoint for getting datasets
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function all ($serverUrl){
        $this->log("*** ALL ***");
        $json = $this->getUrlContent($serverUrl . "/api/search?q=*&type=dataset&sort=name&order=asc&per_page=3");
        if (!$json) {
            $this->log("can not Fetch");
            return;
        }
        $json = json_decode($json, true);
        if ($json === null) {
            $this->log("null Fetched");
            return;
        }


        $datasets = array();
        foreach ($json['data']['items'] as $dataset) {
            array_push($datasets, [$dataset['name'], $dataset['global_id']]);
        }

        $result = [
            "datasets" => $datasets,
            ];
        
        $js = new JSONResponse($result);
        return $js;

    }

    /**
     * request endpoint for getting writable datasets
     * @return JSONResponse
     * @NoAdminRequired
     */

    public function writable ($serverUrl, $serverId, $userToken) {
        $this->log("serverid is" . $serverId . "user token is " . $userToken);
        $datasets = array();
        $message = "";
        $i = 0;
        $pages_nbre = 0;
        do {
            $json = $this->getUrlContent($serverUrl ."/api/mydata/retrieve?role_ids=1&role_ids=6&role_ids=7&dvobject_types=Dataset&published_states=Published&published_states=Unpublished&published_states=Draft&selected_page=" . urlencode($i+1) . "&key=" . urlencode($userToken));
            if (!$json) {
                return;
            }
            $json = json_decode($json, true);
            if ($json === null) {
                $this->log("null Fetched for page " . $i);
                return;
            }
            $success = $json['success'];
            if( !$success){
                $message = $json['error_message'];
                break;
            }
            
            if($i == 0){
                $pages_nbre = $json['data']['pagination']['pageCount'];
                $this->log("pageCount is **** " . $pages_nbre . " here ");
            }

            foreach ($json['data']['items'] as $dataset) {
                array_push($datasets, [$dataset['name'], $dataset['global_id'], $dataset['is_published']]);
            }
        } while(++$i < $pages_nbre);

        $result = [
            "datasets" => $datasets,
            "success"  => $success,
            "message" => $message
            ];
        
        $js = new JSONResponse($result);
        return $js;

    }
    /**
     * check in the db if the file with the same content is already sent
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function exist($serverId, $datasetName, $filesId, $datasetId) {
        $this->log("serverid is " . $serverId . "datasetname is " . $datasetName . " fileids is " . strval($filesId[0]) . " datasetid " . strval($datasetId) . " userid " .  $this->userId);
        $md5List = $this->getMd5List($filesId);
        $sentFiles = $this->mapper->sentFiles($serverId, $datasetName, $filesId, $this->userId, $md5List, $datasetId);
        
        $js = new JSONResponse($sentFiles);
        $this->log("result has length*** " . count($sentFiles));
        return $js;

    }
    public function getMd5List($filesId){
        $md5List =  array();
        foreach ($filesId as $fileId) {
            $md5 = $this->getMd5($fileId);
            $md5List[$fileId]=$md5;
        }
        return $md5List;
    }
    public function getMd5($fileId){

        Filesystem::init($this->userId, '/');
        $view = Filesystem::getView();
        $path = Filesystem::getPath($fileId);
        $this->log("*** path is " . $path);
        $has_access = Filesystem::isReadable($path);
        if ($has_access) {
            return $view->hash("md5",$path);
        }
        return 0;
    }
    /**
     * check in the db of the file with the same id was sent
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function sentFilenames($filesId) {
        $this->log(" sentFilenames is " . strval($filesId[0]));
        $sentFiles = $this->mapper->sentFilenames($filesId);
        
        $js = new JSONResponse($sentFiles);
        $this->log("result has length*** " . count($sentFiles));
        return $js;

    }

}