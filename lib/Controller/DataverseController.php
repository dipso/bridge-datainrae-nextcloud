<?php
namespace OCA\DataverseBridge\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use Psr\Log\LoggerInterface;

class DataverseController extends Controller {

    private $userId;
    private $logger;

	public function __construct($AppName, IRequest $request, $UserId,  LoggerInterface $logger){
		parent::__construct($AppName, $request);
		$this->userId = $UserId;
        $this->logger = $logger;
	}

    public function log($message) {
        $this->logger->error($message, ['extra_context' => 'my extra context']);
    }

    public function getUrlContent($url)
    {
        $ch = curl_init();
        if ($ch === false) {
            throw new Exception('failed to initialize');
        }
        $config = array(
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_URL => $url,
            CURLOPT_REFERER => $url,
            CURLOPT_RETURNTRANSFER => true
        );

        curl_setopt_array($ch, $config);

        $result = curl_exec($ch);
        curl_close($ch);
        if (!$result) {
            $this->log(curl_error($ch), curl_errno($ch));
            return false;
        } else {
            return $result;
        }
    }
      /**
     * request endpoint for getting dataverses
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function all ($serverUrl){
        $this->log("*** ALL ***");
        $json = $this->getUrlContent($serverUrl . "/api/search?q=*&type=dataverse&sort=name&order=asc&per_page=2");
        if (!$json) {
            $this->log("can not Fetch");
            return;
        }
        $dataverses = json_decode($json, true);
        if ($dataverses === null) {
            $this->log("null Fetched");
            return;
        }

        $this->log("**** fetched " . $dataverses);

        $dataverses_id = array();
        foreach ($dataverses['data']['items'] as $dataverse) {
            array_push($dataverses_id, $dataverse['identifier']);
        }

        $result = [
            "dataverses" => $dataverses_id,
            ];
        
        $js = new JSONResponse($result);
        return $js;

    }
}