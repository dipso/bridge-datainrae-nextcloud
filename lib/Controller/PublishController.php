<?php
namespace OCA\DataverseBridge\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use Psr\Log\LoggerInterface;
use OC\Files\Filesystem;
use OCP\Files\IRootFolder;
use \OCP\Files\Node;
use \OCP\Files\Mount\IMountPoint;
use OCA\DataverseBridge\Model\UploadedFile;
use OCA\DataverseBridge\Model\UploadedFileMapper;



class PublishController extends Controller {

    const NEW_UPLOAD = -1;
    const TMP_DIR = '/tmp/';

    private $userId;
    private $logger;
    private $mapper;
    /** @var IRootFolder */
    private $rootFolder;

	public function __construct($AppName, IRequest $request, $UserId,  LoggerInterface $logger,  IRootFolder $rootFolder, UploadedFileMapper $mapper){
		parent::__construct($AppName, $request);
		$this->userId = $UserId;
        $this->logger = $logger;
        $this->rootFolder = $rootFolder;
        $this->mapper = $mapper;
	}

    public function log($message) {
        $this->logger->error($message, ['extra_context' => 'my extra context']);
    }

    public function toStr($var) {
        ob_start();
        var_dump($var);
        return ob_get_clean();
    }

    public function createTmpFile($filename,  $content){
        $tmpPath = '/tmp/' . $filename;
        $tmpfile = fopen($tmpPath,"w");
        fwrite($tmpfile, $content);
        fclose($tmpfile);
        return $tmpPath;
    }

    public function deleteTmpFile($tmpPath){
        if(file_exists($tmpPath)){
            unlink($tmpPath);
        }
    }

    public function getFileMetadata($serverUrl, $dataversefileId, $state, $userToken=null){
        $metadata = "{}";
        if($state == 'published'){
            $metadata = shell_exec("curl -s '$serverUrl/api/files/$dataversefileId/metadata'");
        }
        else {
            $metadata = shell_exec("curl -s -H \"X-Dataverse-key:$userToken\" '$serverUrl/api/files/$dataversefileId/metadata/draft'");
        }
        return $metadata;      
    }

    public function publishFile($serverUrl, $serverId, $userToken, $fileId, $persistentId, $datasetName, $dataversefileId, $state){
        $this->log("in publish one file");
        $this->log("serverid is " . $serverId . " user token is " . $userToken . " file id is " . $fileId);
        $metadata ="{}";
        $newdataverseFileId = self::NEW_UPLOAD;
        Filesystem::init($this->userId, '/');
        $view = Filesystem::getView();
        $path = Filesystem::getPath($fileId);
        $this->log("*** path is " . $path);
        $has_access = Filesystem::isReadable($path); 

        if ($has_access) {
            $source = $view->fopen($path, 'r');
             $filename = basename($path);
            $tmpPath = self::TMP_DIR . $filename;
            \file_put_contents($tmpPath, $source);
            $this->log("*** tmppath is " .  $tmpPath);
            if($dataversefileId != self::NEW_UPLOAD && isset($dataversefileId) ){
                $metadata = $this->getFileMetadata($serverUrl, $dataversefileId, $state, $userToken);
            }
            if($dataversefileId != self::NEW_UPLOAD && $state =='published'){
                $this->log("*** Replaceee " . $filename);
                $this->log("*** METADATA ARE " . $metadata);
                $this->log("*** dataversefileId is " . $dataversefileId);
                $json = shell_exec("curl -v --resolve -X POST -F \"file=@$tmpPath\" -F 'jsonData=$metadata' '$serverUrl/api/files/$dataversefileId/replace?key=$userToken'");
            }
            else
            {
                $this->log("*** create " . $filename);
                $this->log("*** METADATA ARE " . $metadata);
                $json = shell_exec("curl -s -X POST -F \"file=@$tmpPath\" -F 'jsonData=$metadata' '$serverUrl/api/v1/datasets/:persistentId/add?persistentId=$persistentId&key=$userToken'"); 

            }
            $this->deleteTmpFile($tmpPath);
            $this->log("***json 1 " . $json);
            if (!$json || $json == "{}" ) {
                $this->log("can not Fetch ");
                return;
            }
            $json = json_decode($json, true);
            if ($json === null) {
                $this->log("null Fetched ");
                return;
            }
        }
        if($dataversefileId != self::NEW_UPLOAD && $state =='draft'){
            $json['dup_file_msg'] = "A file with the same name is already in the same dataset";
        }
        $newdataverseFileId = $json['data']['files'][0]['dataFile']['id'];
        $this->log("***file id in dataverse DB is " . $newdataverseFileId);
        $md5 =   $view->hash("md5",$path);
        $this->persistSentFile($fileId, $filename ,$md5, $serverId, $persistentId, $datasetName,  $newdataverseFileId, $state);
        $this->notifySuccess($fileId, $filename, $persistentId, $datasetName, $serverUrl);
        return $json;
    }

    public function notifySuccess($fileId, $filename, $persistentId, $datasetName, $serverUrl){
        $manager = \OC::$server->get(\OCP\Notification\IManager::class);
        $shouldFlush = $manager->defer();
        $notification = $manager->createNotification();
        $notification->setApp('dataversebridge')
        ->setUser($this->userId)
        ->setDateTime(new \DateTime())
        ->setObject('upload', $fileId)
        ->setSubject('success_upload', ['filename' => $filename, 'fileId' => $fileId, 'datasetId' => $persistentId, 'dataset' => $datasetName, 'serverUrl' => $serverUrl]);
        $manager->notify($notification);
        if ($shouldFlush) {
            $manager->flush();
        }
    }

    public function persistSentFile($fileId, $filename ,$md5, $serverId, $persistentId, $datasetName, $dataverseFileId,  $state){
        $uploadedFile = new UploadedFile();

        $uploadedFile->setFileId($fileId);
        $uploadedFile->setFilename($filename);
        $uploadedFile->setMd5($md5);
        $uploadedFile->setTargetServerId($serverId);
        $uploadedFile->setUserId($this->userId);
        $uploadedFile->setDatasetId($persistentId);
        $uploadedFile->setDatasetName($datasetName);
        $uploadedFile->setUploadDate(strval(time()));
        $uploadedFile->setDataverseFileId($dataverseFileId);
        $uploadedFile->setDatasetState($state);

        $this->mapper->insert($uploadedFile);

    }

    /**
     * upload files to dataverse
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function publishFiles($serverUrl, $serverId, $userToken, $fileIds, $persistentId, $datasetName, $state){
        $this->log("in publish filesss");
        $this->log("in publish filesss " . $serverId);
        $this->log("in publish stateeee " . $state);
        $responses = array();
        $filenames = $this->getFilenames($fileIds);
        $files = $this->checkForReplace($serverId, $this->userId, $filenames, $persistentId, $state);
        foreach ($fileIds as $fileId) {
            array_push($responses, $this->publishFile($serverUrl, $serverId, $userToken, $fileId, $persistentId, $datasetName, $files[$fileId], $state));
            $this->log("file id publish filesss " . $fileId);
        }
        $js = new JSONResponse($responses);
        return $js;
    }

    public function checkForReplace($serverId, $userId, $filenames, $persistentId, $state){
        $files = array();
        $this->log("***filesToReplace serverid " . $serverId  . " this->userId " . $this->userId . " filename_one " . $filenames[array_keys($filenames)[0]] . " persistentId " . $persistentId . " state " . $state);
        $filesToReplace = $this->mapper->filesToReplace($serverId, $this->userId, $filenames, $persistentId, $state);
        foreach ($filenames as $fileId => $filename){
            $files[$fileId] = self::NEW_UPLOAD;
            if(array_key_exists($fileId, $filesToReplace))
            {
                $files[$fileId] = $filesToReplace[$fileId];
            }
        }
        return $files;
    }
    public function getFilenames($fileIds){
        $filenames = array();
        foreach ($fileIds as $fileId){
            Filesystem::init($this->userId, '/');
            $view = Filesystem::getView();
            $path = Filesystem::getPath($fileId);
            $filenames[$fileId] = basename($path);
        }
        return $filenames;
    }





}