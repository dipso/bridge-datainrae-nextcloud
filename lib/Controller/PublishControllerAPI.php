<?php
namespace OCA\DataverseBridge\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use Psr\Log\LoggerInterface;
use OC\Files\Filesystem;
use OCP\Files\IRootFolder;

class PublishController extends Controller {
    private $userId;
    private $logger;

    /** @var IRootFolder */
    private $storage;

	public function __construct($AppName, IRequest $request, $UserId,  LoggerInterface $logger,  IRootFolder $storage){
		parent::__construct($AppName, $request);
		$this->userId = $UserId;
        $this->logger = $logger;
        $this->storage = $storage;
	}

    public function log($message) {
        $this->logger->error($message, ['extra_context' => 'my extra context']);
    }

    public function getUrlContent($url, $pathToFile, $persistentId, $userToken)
    {
        $ch = curl_init();
        $this->log("*** after init ***");
        if ($ch === false) {
            throw new Exception('failed to initialize');
        }
     
        $persId = 'doi:10.70112/IT2WDA';
        $data = array('file' => new \CURLFile(realpath($pathToFile),  mime_content_type($pathToFile), basename($pathToFile)));
        $fp = fopen($pathToFile, 'r');
        $config = array(
            CURLOPT_URL => "https://data-preproduction.inrae.fr/api/datasets/:persistentId/add?persistentId=$persId",
            CURLOPT_SAFE_UPLOAD => true,
            CURLOPT_VERBOSE => true,
            CURLOPT_PUT => 1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_INFILE => $fp ,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_BUFFERSIZE => 128,
            CURLOPT_INFILESIZE => filesize($pathToFile),
            CURLOPT_HTTPHEADER => array(
              'Accept: application/json',
               "X-Dataverse-Key: $userToken",
               'content-type: multipart/form-data'
          )
        );
        $headers = array(
            "X-Dataverse-Key: $userToken",
            "content-type: multipart/form-data",
        );

        $this->log("*** after set option ***");
        
        curl_setopt_array($ch, $config);

        $result = curl_exec($ch);


        $this->log("*** after exec ***");
        if (curl_errno($ch)) {
            $this->log("*** curl error ***" . curl_error($ch));
        }
        curl_close($ch);
        if (!$result) {
            $this->log("*** false ***");
            $this->log("error : " . curl_error($ch) . " " . curl_errno($ch));
            return false;
        } else {
            $this->log("*** result *** " . $result);
            return $result;
        }
    }

    public function createTmpFile($filename,  $content){
        $tmpPath = '/tmp/' . $filename;
        $tmpfile = fopen($tmpPath,"w");
        fwrite($tmpfile, $content);
        fclose($tmpfile);
        return $tmpPath;
    }

    public function deleteTmpFile($tmpPath){
        if(file_exists($tmpPath)){
            unlink($tmpPath);
        }
    }

    public function publishFile($serverId, $userToken, $fileId, $persistentId){
        $this->log("in publish one file");
        $query_params = array('persistentId' => $persistentId, 'key' => $userToken);
        $baseUrl = 'https://data-preproduction.inrae.fr/api/datasets/:persistentId/add?';

        $this->log("serverid is " . $serverId . " user token is " . $userToken . " file id is " . $fileId . " url is " . $url);
        Filesystem::init($this->userId, '/');
        $view = Filesystem::getView();
        $path = Filesystem::getPath($fileId);
        $this->log("*** path is " . $path);
        $has_access = Filesystem::isReadable($path);

        if ($has_access) {
            $content = $view->file_get_contents($path);
            $this->log("*** content is " .  $content);

            $tmpPath = $this->createTmpFile(basename($path),  $content);
            $this->log("*** tmpPathhhh is " . $tmpPath);
             $json  = $this->getUrlContent($baseUrl . 'persistentId=' . $persistentId, $tmpPath, $persistentId, $userToken);
             $this->deleteTmpFile($tmpPath);
            if (!$json) {
                $this->log("can not Fetch");
                return;
            }
            $json = json_decode($json, true);
            if ($json === null) {
                $this->log("null Fetched");
                return;
            }
            $this->log("**** fetched " . $json);
        }
        return $json;
    }

    /**
     * upload files to dataverse
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function publishFiles($serverId, $userToken, $fileIds, $persistentId){
        $this->log("in publish filesss");
        $responses = array() ;
        for($i=0; $i<count($fileIds); $i++) {

            array_push($responses, $this->publishFile($serverId, $userToken, $fileIds[$i], $persistentId));
        } 
        return new JSONResponse($responses);
    }
}