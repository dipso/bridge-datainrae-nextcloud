<?php

namespace OCA\DataverseBridge\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCA\DataverseBridge\Model\Server;
use OCA\DataverseBridge\Model\ServerMapper;
use OCP\IRequest;
use Psr\Log\LoggerInterface;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OC\Files\Filesystem;
use OCP\Files\IRootFolder;
use \OCP\Files\Node;
use \OCP\Files\Mount\IMountPoint;


class ServerController extends Controller {
    private $userId;
    private $mapper;
    private $logger;

    public function __construct(
        $appName,
        IRequest $request,
        ServerMapper $mapper,
        $userId,
        LoggerInterface $logger
    ) {
        parent::__construct($appName, $request, $mapper);
        $this->mapper = $mapper;
        $this->userId = $userId;
        $this->logger = $logger;
    }
    public function log($message) {
        $this->logger->error($message, ['extra_context' => 'my extra context']);
    }

    public function listServers() {
        return $this->mapper->findAll();
    }
    /**
     * @NoAdminRequired
     **/
    public function saveServers($servers) {
        $this->log("saving " . count($servers));
        foreach($servers as $server) {
            if (array_key_exists('id', $server)) {
                $old = $this->mapper->find($server['id']);//here to fetch from the db
                $old->setName($server['name']);
                $old->setPublishUrl($server['publishUrl']);
                $old->setSizeLimit($server['uploadLimit']);
                $this->mapper->update($old);
            } else {
                $newServer = new Server();
                $newServer->setName($server['name']);
                $newServer->setPublishUrl($server['publishUrl']);
                $newServer->setSizeLimit($server['uploadLimit']);
                $this->log("limit size url " . $newServer->getSizeLimit());
                $this->mapper->insert($newServer);
            }
        }
        return $this->mapper->findAll();
    }

     /**
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function uploadLimit($serverId, $filesId) {
        $this->log("*** UPLOAD  LIMIT is " . $serverId);
        $server = $this->mapper->find($serverId);
        $serverLimit = $server->getSizeLimit();
        $filesSize = $this->getFileSize($filesId);
        $limitExceeded = array();
        foreach ($filesId as $fileId) {
            if($filesSize[$fileId][0]>$serverLimit){
               $limitExceeded[$fileId] =  $filesSize[$fileId][1];
            } 
        }
        return new JSONResponse(['limitExeeded' => $limitExceeded, 'serverLimit' => $serverLimit]);

    }
    public function getFileSize($filesId) {
        $filesSize = array();
        foreach ($filesId as $fileId) {
            Filesystem::init($this->userId, '/');
            $view = Filesystem::getView();
            $path = Filesystem::getPath($fileId);
            $this->log("*** path is " . $path);
            $has_access = Filesystem::isReadable($path);
            if ($has_access) {
                $filename = basename($path);
                $size = $view->filesize($path);
                $this->log("*** UPLOAD  LIMIT is SIZEE " . $size);
                $filesSize[$fileId] = array($size, $filename);
            }
        }
        return $filesSize;
    }
    /**
     * @NoAdminRequired
     **/
    public function deleteServer($id) {
        $this->mapper->delete($this->mapper->find($id));
    }

      /**
     * request DB to get the url based on the servername
     * @return JSONResponse
     * @NoAdminRequired
     */

    public function getServerUrl(string $serverName) {
        $this->log("***getServerUrl*** ");
        $this->log("***getServerUrl*** " . $serverName);
        $serverEntity = $this->mapper->getServerUrl($serverName);
        $this->log("serverUrlEntity*** " . $serverEntity);

        $result = [
            "serverUrl" => $serverEntity->getPublishUrl(),
            "serverId" => $serverEntity->getId(),
            ];


            $this->log("result*** " . $result);
            $this->log("urll*** " . $result['serverUrl']);
        
        return new JSONResponse($result);
    
        
    }

     /**
     * request DB to get all servers then return their names
     * @return JSONResponse
     * @NoAdminRequired
     */
    public function getServers() {
        $servers = [];
        $serverEntities = $this->mapper->findAll();
        foreach($serverEntities as $serverEntity) {
            $servers[$serverEntity->getName()] = ['serverId' => $serverEntity->getId(), 'serverUrl' => $this->removeSlash($serverEntity->getPublishUrl())];
        }
        return new JSONResponse($servers);
    
    }
    public function removeSlash($ServerUrl){
        return $ServerUrl[strlen($ServerUrl)-1] == '/' ? substr($ServerUrl, 0, -1) : $ServerUrl;
    }

}