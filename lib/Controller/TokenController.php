<?php

namespace OCA\DataverseBridge\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCA\DataverseBridge\Model\Token;
use OCA\DataverseBridge\Model\TokenMapper;
use OCP\IRequest;
use Psr\Log\LoggerInterface;

class TokenController extends Controller {
    private $userId;
    private $mapper;
    private $logger;

    public function __construct(
        $appName,
        IRequest $request,
        TokenMapper $mapper,
        $userId,
        LoggerInterface $logger
    ) {
        parent::__construct($appName, $request);
        $this->mapper = $mapper;
        $this->userId = $userId;
        $this->logger = $logger;
    }
    public function log($message) {
        $this->logger->error($message, ['extra_context' => 'my extra context']);
    }

    public function listServers() {
        return $this->mapper->findAll();
    }
    /**
     * @NoAdminRequired
     **/
    public function saveToken($token) {
        $this->log("saving token to DB");
        if (array_key_exists('token_id', $token)) {
            $old = $this->mapper->find($token['token_id']);//here to fetch from the db
            $old->setServerId($token['server_id']);
            $old->setUserId($this->userId);
            $old->setToken($token['token_value']);
            $token = $this->mapper->update($old);
            $this->log("update token id*** ". $token->id);
        } else {
            $newToken = new Token();
            $newToken->setServerId($token['server_id']);
            $newToken->setUserId($this->userId);
            $newToken->setToken($token['token_value']);
            $token = $this->mapper->insert($newToken);
            $this->log("create token id*** " . $token->id);
        }
        return $token;
    }
    /**
     * @NoAdminRequired
     **/
    public function deleteToken($id) {
        $this->mapper->delete($this->mapper->find($id));
    }

    /**
     * @NoAdminRequired
     **/
    public function getToken(string $serverId) {
        $this->log("getToken*** ");
        $this->log("getToken*** " . $serverId);
        settype($serverId, "integer");
        $tokenEntity =  $this->mapper->getToken($serverId, $this->userId);
        $result = [
            "token" => $tokenEntity->getToken(),
            ];

            $this->log("token*** " . $result['token']);
        
        return new JSONResponse($result);
    }
}