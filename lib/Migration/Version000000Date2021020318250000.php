<?php

declare(strict_types=1);

namespace OCA\DataverseBridge\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000000Date2021020318250000 extends SimpleMigrationStep {

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 * @return null|ISchemaWrapper
	 */
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();

		if (!$schema->hasTable('dataverse_user_token')) {
			$table = $schema->createTable('dataverse_user_token');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
			]);
			$table->addColumn('user_id', 'string', [
				'notnull' => true,
			]);
			$table->addColumn('server_id', 'integer', [
				'notnull' => true,
			]);
			$table->addColumn('token', 'string', [
				'notnull' => true,
                'length' => 64,
                'default' => '',
			]);
			$table->addColumn('expiration_date', 'integer', [
				'notnull' => false,
			]);
            $table->setPrimaryKey(['id']);
		}
		return $schema;
	}

}