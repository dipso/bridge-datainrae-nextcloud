<?php

declare(strict_types=1);

namespace OCA\DataverseBridge\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000000Date2021052516450000 extends SimpleMigrationStep {

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 * @return null|ISchemaWrapper
	 */
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();

		if (!$schema->hasTable('dataverse_sent_file')) {
			$table = $schema->createTable('dataverse_sent_file');
            $table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
			]);
			$table->addColumn('file_id', 'integer', [
				'notnull' => true,
				'default' => -1,
			]);
			$table->addColumn('filename', 'string', [
				'notnull' => true,
				'default' => '',
			]);
			$table->addColumn('md5', 'string', [
				'notnull' => true,
				'default' => '',
			]);
			$table->addColumn('user_id', 'string', [
				'notnull' => true,
				'default' => '',
			]);
			$table->addColumn('target_server_id', 'string', [
				'notnull' => true,
				'default' => '',
			]);
            $table->addColumn('dataset_id', 'string', [
				'notnull' => true,
				'default' => '',
			]);
            $table->addColumn('dataset_name', 'string', [
				'notnull' => true,
				'default' => '',
			]);
            $table->addColumn('upload_date', 'string', [
				'notnull' => false,
				'default' => '',
			]);
			$table->setPrimaryKey(['id']);
		}
		return $schema;
	}

}