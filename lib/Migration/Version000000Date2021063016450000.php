<?php

declare(strict_types=1);

namespace OCA\DataverseBridge\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000000Date2021063016450000 extends SimpleMigrationStep {

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 * @return null|ISchemaWrapper
	 */
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();
        $table = $schema->getTable('dataverse_sent_file');
		$table->addColumn('dataset_state', 'string', [
            'notnull' => false,
            'default' => '',
        ]);
		return $schema;
	}

}