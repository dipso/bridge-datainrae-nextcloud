<?php

namespace OCA\DataverseBridge\Model;
use OCP\AppFramework\Db\Entity;
use JsonSerializable;

class Server extends Entity implements JsonSerializable
{
    protected $name;
    protected $publishUrl;
    protected $sizeLimit;
    public function __construct() {
        $this->addType('id', 'integer');
        $this->addType('name', 'string');
        $this->addType('publishUrl', 'string');
        $this->addType('sizeLimit', 'integer');
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'publishUrl' => $this->publishUrl,
            'sizeLimit' => $this->sizeLimit,
        ];
    }

    public function getPublishUrl(){
        return $this->publishUrl;
    }

    public function getName(){
        return $this->name;
    }

    public function getId(){
        return $this->id;
    }

    public function getSizeLimit(){
        return $this->sizeLimit;
    }

    public function __toString() {
        return "Server with id " . $this->id . " and name " . $this->name . " and publishUrl " . $this->publishUrl . " and sizeLimit " . $this->sizeLimit;
    }
}