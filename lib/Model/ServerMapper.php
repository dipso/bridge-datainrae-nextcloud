<?php


namespace OCA\DataverseBridge\Model;

use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;
use OCP\AppFramework\Db\QBMapper;
use OCP\Util;

class ServerMapper extends QBMapper {
    public function __construct(IDBConnection $db) {
        parent::__construct(
            $db,
            'dataversebridge_server',
            '\OCA\DataverseBridge\Model\Server'
        );
    }

      /**
     * @throws \OCP\AppFramework\Db\DoesNotExistException if not found
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException if more than one result
     */
    public function find(int $id) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
           ->from($this->tableName)
           ->where(
               $qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
           );

        return $this->findEntity($qb);
    }


    public function findAll($limit=null, $offset=null) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
           ->from($this->tableName)
           ->setMaxResults($limit)
           ->setFirstResult($offset);

        return $this->findEntities($qb);
    }

    
    public function getServerUrl(string $serverName) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
           ->from($this->tableName)
           ->where(
               $qb->expr()->eq('name', $qb->createNamedParameter($serverName, IQueryBuilder::PARAM_STR))
           );

        return $this->findEntity($qb);
    }
}