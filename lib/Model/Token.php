<?php

namespace OCA\DataverseBridge\Model;
use OCP\AppFramework\Db\Entity;
use JsonSerializable;

class Token extends Entity implements JsonSerializable
{
    protected $userId;
    protected $serverId;
    protected $token;
    protected $expirationDate;



    public function __construct() {
        $this->addType('id', 'integer');
        $this->addType('userId', 'string');
        $this->addType('serverId', 'integer');
        $this->addType('token', 'string');
        $this->addType('expirationDate', 'integer');
    }

    public function getUserId(){
        return $this->userId;
    }
    public function getToken(){
        return $this->token;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'userId' => $this->userId,
            'serverId' => $this->serverId,
            'token' => $this->token,
            'expirationDate' => $this->expirationDate
        ];
    }

}