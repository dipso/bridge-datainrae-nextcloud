<?php


namespace OCA\DataverseBridge\Model;


use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
//use OCP\AppFramework\Db\Mapper;
use OCP\IDBConnection;
use OCP\Util;

class TokenMapper extends QBMapper {
    public function __construct(IDBConnection $db) {
        parent::__construct(
            $db,
            'dataverse_user_token',
            '\OCA\DataverseBridge\Model\Token'
        );
    }

      /**
     * @throws \OCP\AppFramework\Db\DoesNotExistException if not found
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException if more than one result
     */
    public function find(int $id) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
           ->from($this->tableName)
           ->where(
               $qb->expr()->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT))
           );

        return $this->findEntity($qb);
    }

    public function findUserTokens(string $userId) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
           ->from($this->tableName)
           ->where(
               $qb->expr()->eq('user_id', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
           );

        return $this->findEntities($qb);

       /// $cursor = $qb->execute();
        ///$rows = $cursor->fetch();
        ///$cursor->closeCursor();

       /// return $rows;
    }

    public function getToken($serverId, $userId) {
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
           ->from($this->tableName)
           ->where( 'user_id = ? AND server_id = ?')
           ->setParameter(0, $userId)
           ->setParameter(1, $serverId);
               
        return $this->findEntity($qb);
    }


    public function findAll($limit=null, $offset=null) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
           ->from($this->tableName)
           ->setMaxResults($limit)
           ->setFirstResult($offset);

        return $this->findEntities($qb);
    }

}