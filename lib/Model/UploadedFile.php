<?php

namespace OCA\DataverseBridge\Model;
use OCP\AppFramework\Db\Entity;
use JsonSerializable;

class UploadedFile extends Entity implements JsonSerializable
{
    protected $fileId;
    protected $filename;
    protected $md5;
    protected $userId;
    protected $targetServerId;
    protected $datasetId;
    protected $datasetName;
    protected $uploadDate;
    protected $dataverseFileId;
    protected $datasetState;



    public function __construct() {
        $this->addType('fileId', 'integer');
        $this->addType('filename', 'string');
        $this->addType('md5', 'string');
        $this->addType('userId', 'string');
        $this->addType('targetServerId', 'string');
        $this->addType('datasetId', 'string');
        $this->addType('datasetName', 'string');
        $this->addType('uploadDate', 'string');
        $this->addType('dataverseFileId', 'integer');
        $this->addType('datasetState', 'string');
        
    }

    public function getUserId(){
        return $this->userId;
    }
    public function getFileId(){
        return $this->fileId;
    }

    public function getFilename(){
        return $this->filename;
    }
    public function getMd5(){
        return $this->md5;
    }

    public function getTargetServerId(){
        return $this->targetServerId;
    }
    public function getDatasetId(){
        return $this->datasetId;
    }
    public function getDatasetName(){
        return $this->datasetName;
    }
    public function getUploadDate(){
        return $this->uploadDate;
    }
    public function getDataverseFileId(){
        return $this->dataverseFileId;
    }
    
    public function getDatasetState(){
        return $this->datasetState;
    }
    
    public function jsonSerialize() {
        return [
            'fileId' => $this->fileId,
            'filename' => $this->filename,
            'md5' => $this->md5,
            'userId' => $this->userId,
            'targetServerId' => $this->targetServerId,
            'datasetId' => $this->datasetId,
            'datasetName' => $this->datasetName,
            'uploadDate' => $this->uploadDate,
            'dataverseFileId' => $this->dataverseFileId,
            'datasetState' => $this->datasetState
        ];
    }

}