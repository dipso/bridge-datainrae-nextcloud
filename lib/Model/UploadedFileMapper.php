<?php


namespace OCA\DataverseBridge\Model;


use OCP\AppFramework\Db\QBMapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;
use OCP\Util;

class UploadedFileMapper extends QBMapper {
    private $logger;

    public function __construct(IDBConnection $db) {
        parent::__construct(
            $db,
            'dataverse_sent_file',
            '\OCA\DataverseBridge\Model\UploadedFile'
        );
    }

    public function sentFiles($serverId, $datasetName, $filesId, $userId, $md5List, $datasetId){
        $sentFiles =  array();
        foreach ($filesId as $fileId) {
            $entities = $this->sentFile($userId, $serverId, $fileId, $datasetName, $md5List[$fileId], $datasetId);
            foreach ($entities as $entity) {
                $sentFiles[$fileId]=$entity->getFilename();
            }
        }
        return $sentFiles;
        
    }

    public function sentFile($userId, $serverId, $fileId, $datasetName, $md5, $datasetId){
        
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
           ->from($this->tableName, 'uf')
           ->where( 'uf.user_id = :userId AND uf.target_server_id = :serverId  AND uf.file_id = :fileId AND uf.dataset_name = :datasetName  AND uf.md5 = :md5 AND uf.dataset_id = :datasetId')
           ->setParameter('userId', $userId)
           ->setParameter('serverId', $serverId)
           ->setParameter('fileId', $fileId)
           ->setParameter('datasetName', $datasetName)
           ->setParameter('md5', $md5)
           ->setParameter('datasetId', $datasetId);

        return $this->findEntities($qb);
    }

    public function filesToReplace($serverId, $userId, $filenames, $persistentId, $state){
        $FilesToReplace =  array();
        foreach ($filenames as $fileId => $filename) {
            $entities = $this->fileToreplace($serverId, $userId, $filename, $persistentId, $state);
            foreach ($entities as $entity) {
                $FilesToReplace[$fileId] = $entity->getDataverseFileId();
            }
        }
        
        return $FilesToReplace;
    }

    public function fileToReplace($serverId, $userId, $filename, $persistentId, $datasetState){
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
           ->from($this->tableName, 'uf')
           ->where( 'uf.user_id = :userId AND uf.target_server_id = :serverId  AND uf.filename = :filename AND uf.dataset_id = :persistentId')
           ->setParameter('userId', $userId)
           ->setParameter('serverId', $serverId)
           ->setParameter('filename', $filename)
           ->setParameter('persistentId', $persistentId)
           ->orderBy('id', 'DESC')->setMaxResults(1);

           return $this->findEntities($qb);
    }


    public function findAll($limit=null, $offset=null) {
        $qb = $this->db->getQueryBuilder();

        $qb->select('*')
           ->from($this->tableName)
           ->setMaxResults($limit)
           ->setFirstResult($offset);

        return $this->findEntities($qb);
    }

    public function sentFilenames($filesId){
        $sentFiles =  array();
        foreach ($filesId as $fileId) {
            $entities = $this->sentFilename($fileId);
            foreach ($entities as $entity) {
                $sentFiles[$fileId]=array("filename" => $entity->getFilename(), "serverId" => $entity->getTargetServerId(), "datasetId" => $entity->getDatasetId());
            }
        }
        return $sentFiles;
        
    }

    public function sentFilename($fileId){
        
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')->from($this->tableName, 'uf')->where( 'uf.file_id = :fileId')->setParameter('fileId', $fileId);

        return $this->findEntities($qb);
    }



}