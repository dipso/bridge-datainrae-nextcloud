<?php


namespace OCA\DataverseBridge\Notification;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\IUserManager;
use OCP\L10N\IFactory;
use OCP\Notification\IAction;
use OCP\Notification\IDismissableNotifier;
use OCP\Notification\IManager;
use OCP\Notification\INotification;
use OCP\Notification\INotifier;

class Notifier implements INotifier {

	/** @var IFactory */
	protected $factory;

	/** @var IURLGenerator */
	protected $urlGenerator;

	public function __construct(IFactory $factory,
								IURLGenerator $urlGenerator) {
		$this->factory = $factory;
		$this->urlGenerator = $urlGenerator;
	}

	public function getID(): string {
		return 'dataversebridge';
	}

	public function getName(): string {
		return $this->factory->get('dataversebridge')->t('Dataversebridge');
	}

	/**
	 * @param INotification $notification
	 * @param string $languageCode The code of the language that should be used to prepare the notification
	 * @return INotification
	 * @throws \InvalidArgumentException When the notification was not prepared by a notifier
	 */
	public function prepare(INotification $notification, string $languageCode): INotification {
		if ($notification->getApp() !== 'dataversebridge') {
			throw new \InvalidArgumentException('Unhandled app');
		}


        if ($notification->getSubject() === 'success_upload') {
			return $this->handleSuccessUpload($notification, $languageCode);
		}

        throw new \InvalidArgumentException('Unhandled subject');

    }

    public function  handleSuccessUpload(INotification $notification, string $languageCode): INotification {
        $l = $this->factory->get('dataversebridge', $languageCode);

        $param = $notification->getSubjectParameters();
        $filename = $param['filename']; $dataset = $param['dataset'];
		$datasetLink = $this->datasetLink($param['serverUrl'], $param['datasetId']);

		$subTitleTmp1 = str_replace('{file}', $filename, 'Le fichier {file} a été déposé dans le dataset {dataset}: {datasetLink}');
		$subTitleTmp2 = str_replace('{datasetLink}', $datasetLink, $subTitleTmp1);
        $subTitle = str_replace('{dataset}', $dataset, $subTitleTmp2);

		$notification->setRichSubject(
            $l->t('Fichier {file} déposé avec succès dans Dataverse.'),
            [
                'file' => [
                    'type' => 'highlight',
                    'id' => $param['fileId'],
                    'name' => $filename,
                ],
            ])
            ->setParsedSubject(str_replace('{file}', $filename, $l->t('Fichier {file} déposé avec succès dans Dataverse.')))
			->setRichMessage(
				$l->t($subTitle),
				[
					'dataset' => [
						'type' => 'highlight',
						'id' => $param['fileId'],
						'name' => $dataset,
					]
				])
			->setParsedMessage($l->t($subTitle));

        return  $notification;
    
    }

	public function datasetLink($serverUrl, $persistentId){
		 $dataset_link = '';
        if( str_contains($serverUrl,'-') ){
            $dataset_link = $serverUrl . '/dataset.xhtml?persistentId=' . $persistentId;
		}
		else {
            $dataset_link = "https://doi.org/" + $persistentId;
		}
        return $dataset_link;
	}



}