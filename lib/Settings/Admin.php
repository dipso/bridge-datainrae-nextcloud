<?php



namespace OCA\DataverseBridge\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\Settings\ISettings;
use OCA\DataverseBridge\Model\ServerMapper;


class Admin implements ISettings
{
    /**
     * Nextcloud config container
     *
     * @var IConfig
     */
    private $_config;
    private $mapper;

    /**
     * Constructors construct.
     *
     * @param IConfig $config Nextcloud config container
     */
    public function __construct(IConfig $config, ServerMapper $mapper)
    {
        $this->_config = $config;
        $this->mapper = $mapper;
    }


    /**
     * Create Admin menue content
     *
     * @return TemplateResponse
     */
    public function getForm()
    {
        $servers = $this->mapper->findAll();
        $params = [
            'servers' => $this->mapper->findAll()
        ];

        return new TemplateResponse('dataversebridge', 'settings-admin', $params);
    }

    /**
     * Actual section name to use
     *
     * @return string the section, 'dataversebridge'
     */
    public function getSection() 
    {
        return 'dataversebridge';
    }

    /**
     * Where to show the section
     *
     * @return int 0
     */
    public function getPriority() 
    {
        return 0;
    }
}