<?php



namespace OCA\DataverseBridge\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\Settings\ISettings;
use OCA\DataverseBridge\Model\ServerMapper;
use OCA\DataverseBridge\Model\TokenMapper;

use Psr\Log\LoggerInterface;

class Personal implements ISettings
{
    /**
     * Nextcloud config container
     *
     * @var IConfig
     */
    private $_config;
    private $mapper;
    private $tokenMapper;
    private $logger;

    /**
     * Constructors construct.
     *
     * @param IConfig $config Nextcloud config container
     */
    public function __construct(IConfig $config, ServerMapper $mapper, TokenMapper $tokenMapper, LoggerInterface $logger)
    {
        $this->_config = $config;
        $this->mapper = $mapper;
        $this->tokenMapper = $tokenMapper;
        $this->logger = $logger;
    }

    public function log($message) {
        $this->logger->error($message, ['extra_context' => 'my extra context']);
    }

    /**
     * Create personal menue content
     *
     * @return TemplateResponse
     */
    
    public function getForm()
    {
        $servers = []; $userTokens = [];
        $userId = \OC::$server->getUserSession()->getUser()->getUID();
        $this->log("userId is " . $userId);
        $serverEntities = $this->mapper->findAll();
        $UserTokenEntities = $this->tokenMapper->findUserTokens($userId);

        foreach($UserTokenEntities as $i => $token) {
            $userTokens[$token->getServerId()] = ['id' => $token->getId(), 'userId'  => $userId , 'serverId' => $token->getServerId(), 'token' => $token->getToken()];
        }

        foreach($serverEntities as $i => $s) {
            $servers[$i] = ['id' => $s->getId(), 'name' => $s->getName(), 'publishUrl' => $s->getPublishUrl()];
        }



        $params = [
            'servers' => $servers,
            'usertokens' => $userTokens
        ];

        return new TemplateResponse('dataversebridge', 'settings-personal', $params);
    }

    /**
     * Actual section name to use
     *
     * @return string the section, 'dataversebridge'
     */
    public function getSection() 
    {
        return 'dataversebridge';
    }

    /**
     * Where to show the section
     *
     * @return int 0
     */
    public function getPriority()
    {
        return 0;
    }
}