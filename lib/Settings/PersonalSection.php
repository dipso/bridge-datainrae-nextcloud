<?php

namespace OCA\DataverseBridge\Settings;

use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\Settings\IIconSection;

class PersonalSection implements IIconSection
{
    /** 
     * Different language support
     *
     * @var IL10N
     */
    private $_l;

    /**
     * URL generator
     *
     * @var IURLGenerator 
     * */
    private $_url;

    /**
     * {@inheritdoc}
     *
     * @param IURLGenerator $url URL generator used to link to the image
     * @param IL10N         $l   Language support
     */
    public function __construct(IURLGenerator $url, IL10N $l) 
    {
        $this->_url = $url;
        $this->_l = $l;
    }

    /**
     * {@inheritdoc}
     *
     * @return string the app id, 'dataversebridge'
     */
    public function getID() 
    {
        return 'dataversebridge';
    }

    /**
     * {@inheritdoc}
     *
     * @return string the section, 'dataversebridge'
     */
    public function getName() 
    {
        return 'Dataverse';
    }

    /**
     * {@inheritdoc}
     *
     * @return int the arrang priority, 75
     */
    public function getPriority() 
    {
        return 75;
    }

     /**
      * {@inheritdoc}
      *
      * @return URL of the icon shown in the admin settings
      */
    public function getIcon()
    {
        return $this->_url->imagePath('dataversebridge', 'dataverse.png');
    }
}