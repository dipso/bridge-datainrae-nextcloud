<div class="container">
  <form action="">
    <div class="row">
      <div class="col-25">
        <label for="source">Source File</label>
      </div>
      <div class="col-75">
        <input type="text" id="source" name="source" placeholder="witch file to send to dataverse">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="lname">Destination</label>
      </div>
      <div class="col-75">
        <input type="text" id="destination" name="destination" placeholder="where to store the file in Dataverse..">
      </div>
    </div>
    <div class="row">
      <input type="submit" value="Submit">
    </div>
  </form>
</div>
