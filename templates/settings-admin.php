<?php
script('dataversebridge', 'settings-admin');
?>
<div class="section" id="inrae_dataverse">
    <h2>Dataverse Bridge</h2>
  <div id="added_servers">
    <?php foreach ($servers as $server): ?>
        <div id="dataverse_server_<?php p($server->getId()) ?>">
        <p id="dataverseUrlField_<?php p($server->getId()) ?>">
            <input title="publish_baseurl" type="text" name="publish_baseurl"
                   id="url_<?php p($server->getId()) ?>"
                   placeholder=": https://data.inrae.fr" style="width: 400px"
                   value="<?php p($server->getPublishUrl()); ?>"/>
                   <em>URL du serveur</em>
        </p>
        <p id="dataverseNameField_<?php p($server->getId()) ?>">
            <input title="name_<?php p($server->getId()) ?>" type="text" name="name"
                   id="name_<?php p($server->getId()) ?>"
                   style="width: 400px"
                   value="<?php p($server->getName()); ?>"/>
                   <em>Nom du serveur</em>
        </p>
        <!--getLimitSize -->
        <p id="dataverseLimitSize_<?php p($server->getId())?>">
            <input title="limitSize_<?php p($server->getId()) ?>" type="text" name="limitSize"
                   id="limitSize_<?php p($server->getId()) ?>"
                   style="width: 400px"
                   value="<?php p($server->getSizeLimit()); ?>"/>
                   <em>Taille limite de téléchargement du serveur (Octets)</em>
        </p>
        <button id="#delete_<?php p($server->getId());?>">Supprimer le serveur</button>
        <button id="#update_<?php p($server->getId());?>">Mise à jour du serveur</button>
        </div>
    <?php endforeach ?>
  </div>
    <button id="add-server">Ajouter un nouveau serveur</button>
    <button id="send" style="display: none;">Sauvegarder les changements</button>
    <button id="delete" style="display: none;">Supprimer le serveur</button>
    <div id="saving"><span class="msg"></span><br /></div>
</div>