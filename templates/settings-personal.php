<?php
    script('dataversebridge', 'settings-personal');
?>
<div class="section" id="inrae_dataverse">
    <h2>Dataverse Bridge <span><a class="info_bulle" href="https://ist.blogs.inrae.fr/datainrae-guide/sauthentifier-et-gerer-son-compte/#Espace_Personnel" title="Générer le jeton dans Dataverse en suivant cette documentation" target="_blank"><strong>?</strong></a></span></h2>
   <?php foreach ($servers as $server): ?>
    <?php 
        $serverId=$server['id'];
        $tokenId=$usertokens[$serverId]['id'];
        $tokenValue=$usertokens[$serverId]['token'];
          
    ?>
    <p id="dataverseServerName">
        <input title="serverName" type="text" id="serverName_<?php p($serverId)?>"
               value="<?php p($server['name']); ?>"
               style="width: 400px" disabled/>
        <em>Serveur Data INRAE</em>
    </p>
     <p id="dataverseUrlField">
        <input title="publish_baseurl" type="text" id="dataverseUrl_<?php p($serverId)?>"
               value="<?php p($server['publishUrl']); ?>"
               style="width: 400px" disabled/>
        <em>URL serveur Data INRAE</em>
    </p>
    <p id="dataverseAPITokenField">
        <input title="dataverse API token" type="text" id="dataverse_apitoken_<?php p($serverId)?>" value="<?php p($tokenValue); ?>" name="dataverse_apitoken"
               style="width: 400px" />
        <em>token Data INRAE</em>
        <div id="tokenMessage_<?php p($serverId)?>" style="display: none;"><span class="msg"></span><br /></div>
    </p>
    <p id="dataverseManageAPIToken">
        <button id="dataverse_save_apitoken_<?php p($serverId)?>_<?php p($tokenId)?>" href="#">Save Dataverse Token</button>
        <button id="dataverse_delete_apitoken_<?php p($serverId)?>_<?php p($tokenId)?>" href="#">Delete Dataverse Token</button>
    </p>
    <br />
    <?php endforeach ?>
    <div id="saving"><span class="msg"></span><br /></div>

</div>